# Wellcome to Coolmovies! 🎬


## About this project

Coolmovies is a website for you who want to review a movie. Join us and find out what people think about the movies you like!



## How to run CoolMovies 

First: you have to go coolmovies-frontend folder:
```bash
cd coolmovies-frontend
```


You will need NPM or Yarn, if you doesn't have the last one, run this code:
```bash
npm install --global yarn
```

Now you can install project dependencies:
```bash
yarn 
```
or
```bash
npm install 
```
Turn on the localhost with this command:
```bash
yarn dev
```

## Coolmovies!

Now you can use our web site, go and make your review!

<img src="./coolmovies-frontend/public/img/webSitePrint1.jpg" alt="Coolmovies Print"> 

<img src="./coolmovies-frontend/public/img/webSitePrint2.jpg" alt="Coolmovies Print"> 

<img src="./coolmovies-frontend/public/img/webSitePrint3.jpg" alt="Coolmovies Print"> 

## My final thoughts

It was fun and challenging to do this challenge. I learned new things about Next.js and was able to put my knowledge about responsiveness into practice.
But I wish I could have more time to resolve the CORS problem (that took me so long),finish all http requests and and have created a more complete design.
