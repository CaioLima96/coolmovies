import styles from './styles.module.css'
import {Typography,} from '@mui/material/';

const FooterComp = () => {
    return (

        <footer className={styles.footer}>
            <Typography>{'CoolMovies © 2022'}</Typography>
        </footer>

    )
}

export default FooterComp