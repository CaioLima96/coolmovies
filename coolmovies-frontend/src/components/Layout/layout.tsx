import React from "react";
import HeaderComp from '../HeaderComp';
import FooterComp from '../FooterComp'

interface ILayoutProps {
    children: React.ReactChild
}

const Layout = (props: ILayoutProps) => {
    return (
        <>
            <HeaderComp/>
            {props.children}
            <FooterComp/>
        </>
    )
}

export default Layout