import styles from './styles.module.css'

interface IMainCompProps{
    children: React.ReactNode
}

const MainComp = (props: IMainCompProps) => {

    return (

        <main className={styles.mainContainer}>
            {props.children}
        </main>
        
    )
}

export default MainComp