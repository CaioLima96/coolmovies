import React from 'react';
import {ApolloClient,InMemoryCache,ApolloProvider,useQuery,gql} from "@apollo/client";

import { getAllMovies } from '../postgraphiql/queries/getAllMovies'


const MoviesList = () => {
	const { loading, error, data } = useQuery(getAllMovies);

	if (loading) return <p>Loading...</p>;
	if (error) return <p>Error :(</p>;
	console.log(data)

	return data.map((item) => (

		<p>
			{item.title}
		</p>

	));

}


export default MoviesList