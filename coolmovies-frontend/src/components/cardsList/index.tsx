import styles from './styles.module.css'

const CardsList = () => {
    return (
        <ul className={styles.cardsList}>
            <li>
                <div className={styles.cardsListaImg}>
                    <img src="https://img.elo7.com.br/product/zoom/2C25D38/big-poster-filme-rogue-one-uma-historia-star-wars-90x60-cm-presente-nerd.jpg"/>
                </div>
                <div className={styles.cardsListaInfo}>
                    <p>Rogue One: A Star Wars Story</p>
                    <p>Release date: 2016-12-16</p>
                    <p>Director: Gareth Edwards</p>
                </div>
            </li>
            
            <li>
                <div className={styles.cardsListaImg}>
                    <img src="https://img.elo7.com.br/product/zoom/2C25D38/big-poster-filme-rogue-one-uma-historia-star-wars-90x60-cm-presente-nerd.jpg"/>
                </div>
                <div className={styles.cardsListaInfo}>
                    <p>Star Wars: A New Hope</p>
                    <p>Release date: 1977-05-25</p>
                    <p>Director: George Lucas</p>
                </div>
            </li>
        </ul>
    )
}

export default CardsList