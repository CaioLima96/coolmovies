import {Typography,} from '@mui/material/';
import Link from 'next/link'

import styles from './styles.module.css'


const HeaderComp = () => {

    function myNavBar() {
        let nav = document.getElementById('myNavBar')
        if(nav != null) {
            nav.classList.toggle('hideShowBtn')
        }

        // document.addEventListener("click", myFunc)
    }

    return (

        <header className={styles.header}>
            <Link href="/"><Typography>{'CoolMovies'}</Typography></Link>
            <p className={styles.opeNcloseNav}  onClick={myNavBar}>X</p>
            <nav className={styles.navBar} id="myNavBar">
                <Link href="/"><Typography>{'Home'}</Typography></Link>
                <Link href="/reviews"><Typography>{'Reviews'}</Typography></Link>
                <Link href="/about"><Typography>{'About'}</Typography></Link>
            </nav>
        </header>

    )
}

export default HeaderComp