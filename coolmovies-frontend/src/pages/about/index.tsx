import React from "react";
import { css } from '@emotion/react';
import Image from "next/image"
import MainComp from "../../components/MainComp"

import styles from './styles.module.css'
import img1 from "../../../public/img/movieWallpaper1.jpg"
import img2 from "../../../public/img/movieWallpaper2.jpg"

const About = () => {
    return (
        
        <>
            <h1 css={title.heading}>About Us</h1>

            <MainComp>
                
                <div className={styles.aboutGrid}>


                    <div><Image src={img1}/></div>
                    <p>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    </p>
                    <div className={styles.gap}></div>
                    <div><Image src={img2}/></div>
                    <p>
                        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                    </p>

                </div>
            </MainComp>
        </>
    )
}

const title = {
    heading: css({
        textAlign: 'center',
    }),
}


export default About