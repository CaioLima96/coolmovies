import React from "react";
import { css } from '@emotion/react';

import CardsList from "../components/CardsList";
import MainComp from "../components/MainComp";

const Test = () => {

    return (
        <>
            <h1 css={styles.title}>The best review you will find here!</h1>

            <MainComp>
                <CardsList/>
            </MainComp>
            
        </>
    )
}

const styles = {
    title: css({
        textAlign: 'center',
    }),
}

export default Test

