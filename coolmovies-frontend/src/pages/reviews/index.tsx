import Image from "next/image"
import { useState } from "react";
import MainComp from "../../components/MainComp"


import styles from './styles.module.css'
import StarImg from '../../../public/img/star1.png'


const MovieReview = () => {
    
    const [showEditForm, setEditForm] = useState(false)

    return (
        <MainComp>
            <div className={styles.reviewContainer}>

                <div className={styles.movieInfo}>
                    <div className={styles.movieInfoImg}>
                        <img src="https://img.elo7.com.br/product/zoom/2C25D38/big-poster-filme-rogue-one-uma-historia-star-wars-90x60-cm-presente-nerd.jpg" />
                    </div>
                    <div className={styles.movieInfoText}>
                        <p>Rogue One: A Star Wars Story</p>
                        <p>Release date: 2016-12-16</p>
                        <p>Director: Gareth Edwards</p>
                    </div>
                </div>

                <div className={styles.colBreak}></div>

                <ul className={styles.movieReviewsList}>
                    <li>
                        <div className={styles.movieReviewTitle}>
                            <p>"Best Action Movie"</p>
                            <p>4 </p>
                            <Image src={StarImg} />
                        </div>
                        <div className={styles.movieReviewBody}>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                            <p className={styles.author}>Author: Maria</p>

                            <p onClick={() => setEditForm(!showEditForm)}>Edit</p>
                        </div>

                        {showEditForm ? 

                            <form className={styles.reviewEdit}>

                                <div className={styles.inputField}>
                                    <label htmlFor="title">Review Title</label>
                                    <input id="title" type="text" placeholder="Enter your review title here" required></input>
                                </div>

                                <div className={styles.inputField}>
                                    <label htmlFor="rating">Rating</label>
                                    <input id="rating" type="number" placeholder="Enter your rating here" required></input>
                                </div>

                                <div className={styles.rowBreak}></div>

                                <div className={styles.inputField}>
                                    <label htmlFor="body">Your Review</label>
                                    <textarea id="body" placeholder="Enter your review here" required></textarea >
                                </div>

                                <div className={styles.rowBreak}></div>

                                <button>SUBMIT</button>

                            </form> 
                        
                        : null}

                        
                    </li>

                    <li>
                        <div className={styles.movieReviewTitle}>
                            <p>"Best Action Movie"</p>
                            <p>4 </p>
                            <Image src={StarImg} />
                        </div>
                        <div className={styles.movieReviewBody}>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                            <p className={styles.author}>Author: Maria</p>

                            <p onClick={() => setEditForm(!showEditForm)}>Edit</p>
                        </div>

                        {showEditForm ? 

                            <form className={styles.reviewEdit}>

                                <div className={styles.inputField}>
                                    <label htmlFor="title">Review Title</label>
                                    <input id="title" type="text" placeholder="Enter your review title here" required></input>
                                </div>

                                <div className={styles.inputField}>
                                    <label htmlFor="rating">Rating</label>
                                    <input id="rating" type="number" placeholder="Enter your rating here" required></input>
                                </div>

                                <div className={styles.rowBreak}></div>

                                <div className={styles.inputField}>
                                    <label htmlFor="body">Your Review</label>
                                    <textarea id="body" placeholder="Enter your review here" required></textarea >
                                </div>

                                <div className={styles.rowBreak}></div>

                                <button>SUBMIT</button>

                            </form> 
                        
                        : null}

                        
                    </li>
                </ul>

            </div>
        </MainComp>
    )
}

export default MovieReview