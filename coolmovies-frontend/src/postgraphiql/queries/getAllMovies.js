import { gql } from "@apollo/client";
import { client } from '../../pages/_app'

export const getAllMovies = gql`
	{
		allMovies {
			nodes {
				id
				title
				releaseDate
				movieDirectorByMovieDirectorId {
					id
					name
				}
			}
		}
	}
`;


// export const getAllMovies = client
// 	.query({
// 		query: gql`
//     	{
// 			allMovies {
// 				nodes {
// 					id
// 					title
// 					releaseDate
// 					movieDirectorByMovieDirectorId {
// 						id
// 						name
// 					}
// 				}
// 			}
// 		}
//     `
// 	})
// 	.then(result => console.log(result));